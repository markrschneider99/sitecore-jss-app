const LINK_VARIANTS = {
  link: 'font-bold text-theme-button hover:underline',
  button:
    'bg-theme-button text-black p-2 font-bold rounded-md text-center border-0 border-b-2 border-theme-button-alt hover:bg-theme-button-alt hover:underline',
};

const MyLink = ({ href, text, variant = 'link' }) => {
  return (
    <a href={href} className={LINK_VARIANTS[variant]}>
      {text}
    </a>
  );
};

export default MyLink;

// Global
import { createContext, useState } from 'react';
// Local
import MyInput from './MyInput';

export const MyFormContext = createContext({
  values: { firstName: '', lastName: '', email: '' },
  setValues: () => null,
});

const MyForm = () => {
  const [values, setValues] = useState({
    firstName: 'Phillup',
    lastName: 'McCoffee',
    email: 'grandelatte@stablox.biz',
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(values);
  };

  return (
    <MyFormContext.Provider value={{ values, setValues }}>
      <form onSubmit={handleSubmit}>
        <MyInput name="firstName" label="First Name" />
        <MyInput name="lastName" label="Last Name" />
        <MyInput name="email" label="Email" />
        <button type="submit">Submit</button>
      </form>
    </MyFormContext.Provider>
  );
};

export default MyForm;

import { authorableComponent } from './scripts/scaffold/templates/authorable-component.mjs';
import { helperComponent } from './scripts/scaffold/templates/helper-component.mjs';
import { jest } from './scripts/scaffold/templates/jest.mjs';
import { mockData } from './scripts/scaffold/templates/mock-data.mjs';
import { storybook } from './scripts/scaffold/templates/storybook.mjs';

export const config = {
  projectType: 'JSS',
  scaffold: {
    subdirectories: [
      {
        path: './components/authorable/general',
        name: 'General',
        dataComponent: 'authorable/general/',
        storybook: 'Authorable/General/',
      },
      {
        path: './components/authorable/hero',
        name: 'Hero',
        dataComponent: 'authorable/hero/',
        storybook: 'Authorable/Hero/',
      },
      {
        path: './components/authorable/layout',
        name: 'Layout',
        dataComponent: 'authorable/layout/',
        storybook: 'Authorable/Layout/',
      },
      {
        path: './components/authorable/listing',
        name: 'Listing',
        dataComponent: 'authorable/listing/',
        storybook: 'Authorable/Listing/',
      },
      {
        path: './components/authorable/promo',
        name: 'Promo',
        dataComponent: 'authorable/promo/',
        storybook: 'Authorable/Promo/',
      },
      {
        path: './components/authorable/site',
        name: 'Site',
        dataComponent: 'authorable/site/',
        storybook: 'Authorable/Site/',
      },
      {
        path: './components/helpers',
        name: 'Helpers',
        dataComponent: 'helpers/',
        storybook: 'Helpers/',
        templates: {
          '[name].tsx': helperComponent,
        },
      },
    ],
    templates: {
      '[name].mock-data.ts': mockData,
      '[name].stories.tsx': storybook,
      '[name].test.js': jest,
      '[name].tsx': authorableComponent,
    },
  },
};

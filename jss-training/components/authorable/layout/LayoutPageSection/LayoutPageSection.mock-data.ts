// Lib
import { Theme } from 'lib/get-theme';
import { getSampleRenderingContext } from 'lib/mocks/mock-placeholder';

const defaultData = {
  rendering: {
    ...getSampleRenderingContext('hztl-layout-page-section'),
    componentName: 'LayoutPageSection',
  },
  fields: {
    sectionTheme: {
      fields: {
        Value: {
          value: 'theme-inherit' as Theme,
        },
      },
    },
  },
};

export default defaultData;

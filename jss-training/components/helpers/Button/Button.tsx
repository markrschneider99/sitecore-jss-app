// Global
import { LinkField } from '@sitecore-jss/sitecore-jss-nextjs';
import classNames from 'classnames';
// Components
import LinkWrapper from '../LinkWrapper/LinkWrapper';

type ButtonVariant = 'primary' | 'secondary';

interface ButtonProps {
  children?: React.ReactNode | React.ReactNode[];
  field?: LinkField;
  onClick?: (event: React.MouseEvent | React.TouchEvent) => void;
  type?: 'button' | 'submit';
  variant?: ButtonVariant;
}

const buttonBaseClasses =
  'font-bold align-middle appearance-none inline-block items-start leading-none min-h-0 no-underline text-center inline-flex items-center';

const buttonClasses: Record<ButtonVariant, string> = {
  primary: classNames(
    buttonBaseClasses,
    'bg-theme-btn-bg',
    'text-theme-btn-text',
    'px-11',
    'py-4',
    'hover:bg-theme-btn-bg-hover'
  ),
  secondary: classNames(buttonBaseClasses, 'border-theme-text', 'border-2', 'px-10', 'py-3'),
};

const Button = ({
  children,
  field,
  onClick,
  type = 'button',
  variant = 'primary',
}: ButtonProps): JSX.Element => {
  if (!!field) {
    return <LinkWrapper field={field} className={buttonClasses[variant]} />;
  }

  return (
    <button type={type} onClick={onClick} className={buttonClasses[variant]}>
      <>{children}</>
    </button>
  );
};

export default Button;

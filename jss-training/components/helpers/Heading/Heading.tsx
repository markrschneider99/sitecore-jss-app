// Global
import { Field, Text } from '@sitecore-jss/sitecore-jss-nextjs';
import HeadingTag, { HeadingLevel } from '@dullaghan/heading-tag';
import classNames from 'classnames';

type HeadingSize = 'sm' | 'lg' | 'xl';

interface HeadingProps {
  field: Field<string>;
  level?: HeadingLevel;
  offset?: number;
  size?: HeadingSize;
  className?: string;
}

const headingClasses: Record<HeadingSize, string> = {
  sm: 'font-black text-base md:text-xl leading-tight',
  lg: 'font-black text-xl md:text-4xl leading-tight',
  xl: 'font-black text-3xl md:text-5xl leading-none',
};

const Heading = ({
  field,
  className,
  level = 2,
  offset = 0,
  size = 'lg',
}: HeadingProps): JSX.Element => (
  <HeadingTag className={classNames(headingClasses[size], className)} level={level} offset={offset}>
    <Text field={field} tag="" />
  </HeadingTag>
);

export default Heading;

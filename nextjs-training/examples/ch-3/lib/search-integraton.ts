import MockAPI, { MockSearchArgs } from '../third-party/mock-api';

const search = async (args: MockSearchArgs = {}) => {
  return new MockAPI(process.env.SEARCH_API_KEY as string).search(args);
};

export default search;

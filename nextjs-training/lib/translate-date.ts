const locale = typeof navigator !== 'undefined' ? navigator.language : 'en-US';

// Return a date represented numerically based on the user's language preferences
const translateDate = (epochMs: number): string =>
  new Intl.DateTimeFormat('en-US', {
    day: 'numeric',
    month: 'numeric',
    year: '2-digit',
  }).format(new Date(epochMs));

export default translateDate;
